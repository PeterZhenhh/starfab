# UI Widget Hooks
GEOMETRY_PREVIEW_WIDGET = (
    "starfab.widget.geom_preview"  # called with the geometry P4KInfo
)
COLLAPSABLE_GEOMETRY_PREVIEW_WIDGET = (
    "starfab.widget.collapsable_geom_preview"  # called with the geometry P4KInfo
)
